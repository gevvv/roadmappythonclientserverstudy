# RoadmapPythonClientServerStudy

### Написать пару примеров HTTP серверов

##### Первая задача
* Запустить сервер который будет возвращать страницу с информацией сколько запросов было на сервер
* Простой счетсик запросов

##### Вторая задача
* Запустить сервер который будет возвращать страницу с информацией сколько запросов было на сервер по кажому пути
* Пример: есть страница "/stats" которая возвращает список путей и количество запросов по каждому
```html
/name           1
/test           3
/some           2
/stats          15
```
##### Research задачи
* Поискать и повторить локально примеры CLI (Command Line Interfaces)

###### Resources:
* [http.server: a simple HTTP server module in python 3](https://youtu.be/4boogIZa1ko)
* [Create a simple http server with Python 3(]https://daanlenaerts.com/blog/2015/06/03/create-a-simple-http-server-with-python-3/)
* [Simple Python Web Server](https://youtu.be/hFNZ6kdBgO0)

### Повторение Python
##### [Python для Начинающих](https://www.youtube.com/playlist?list=PLg5SS_4L6LYtHCActBzbuGVYlWpLYqXC6)
 * [Переменные](https://youtu.be/_1cKppmkAyA)
 * [Строки](https://youtu.be/O4eWLvt0_n8)
 * [Номера](https://youtu.be/gAuTP0vYBqE)
 * [Циклы](https://youtu.be/4ebJdrx4F1E)
 * [Массивы Часть 1](https://youtu.be/Rnp3bt7XSkA)
 * [Массивы Часть 2](https://youtu.be/50Xz_9dahlQ)
 * [Словари Часть 1](https://youtu.be/P6YT1Ka2YFo)
 * [Словари Часть 2](https://youtu.be/DneUKPJ0OZw)
 * [Аргументы коммандной строки](https://youtu.be/FBg5ThfzDjU)

 ##### [Python Tutorials](https://www.youtube.com/playlist?list=PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU)
 * [Lists, Tuples, and Sets](https://youtu.be/W8KRzm-HUcc)
 * [Dictionaries — Working with Key-Value Pairs](https://youtu.be/daefaLgNkw0)
 * [Sorting Lists, Tuples, and Objects](https://youtu.be/D3JvDWO-BY4)
